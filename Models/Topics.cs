﻿using System;
using System.Collections.Generic;

namespace LocalSearchEngineAPI.Models
{
    public partial class Topics
    {
        public Topics()
        {
            Places = new HashSet<Places>();
            Subscriptions = new HashSet<Subscriptions>();
        }

        public int Id { get; set; }
        public string TopicDescription { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Places> Places { get; set; }
        public virtual ICollection<Subscriptions> Subscriptions { get; set; }
    }
}
