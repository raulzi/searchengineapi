﻿using System;
using System.Collections.Generic;

namespace LocalSearchEngineAPI.Models
{
    public partial class Places
    {
        public int Id { get; set; }
        public string PlaceName { get; set; }
        public string PlaceDescription { get; set; }
        public string Address { get; set; }
        public string ImageB64 { get; set; }
        public int? IdTopic { get; set; }
        public int? IdLocality { get; set; }
        public bool Active { get; set; }

        public virtual Cities IdLocalityNavigation { get; set; }
        public virtual Topics IdTopicNavigation { get; set; }
    }
}
