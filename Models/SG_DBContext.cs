﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LocalSearchEngineAPI.Models
{
    public partial class SG_DBContext : DbContext
    {
        public SG_DBContext()
        {
        }

        public SG_DBContext(DbContextOptions<SG_DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<Places> Places { get; set; }
        public virtual DbSet<States> States { get; set; }
        public virtual DbSet<Subscriptions> Subscriptions { get; set; }
        public virtual DbSet<Topics> Topics { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Cities>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CityKey)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Cities)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK__Cities__StateId__3A81B327");
            });

            modelBuilder.Entity<Places>(entity =>
            {
                entity.Property(e => e.ImageB64)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.PlaceDescription)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PlaceName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdLocalityNavigation)
                    .WithMany(p => p.Places)
                    .HasForeignKey(d => d.IdLocality)
                    .HasConstraintName("FK__Places__IdLocali__4AB81AF0");

                entity.HasOne(d => d.IdTopicNavigation)
                    .WithMany(p => p.Places)
                    .HasForeignKey(d => d.IdTopic)
                    .HasConstraintName("FK__Places__IdTopic__4BAC3F29");
            });

            modelBuilder.Entity<States>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abbreviation)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.StateKey)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Subscriptions>(entity =>
            {
                entity.HasKey(e => new { e.IdUser, e.IdTopic });

                entity.HasOne(d => d.IdTopicNavigation)
                    .WithMany(p => p.Subscriptions)
                    .HasForeignKey(d => d.IdTopic)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Subscript__IdTop__47DBAE45");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Subscriptions)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Subscript__IdUse__46E78A0C");
            });

            modelBuilder.Entity<Topics>(entity =>
            {
                entity.Property(e => e.TopicDescription)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });
        }
    }
}
