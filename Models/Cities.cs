﻿using System;
using System.Collections.Generic;

namespace LocalSearchEngineAPI.Models
{
    public partial class Cities
    {
        public Cities()
        {
            Places = new HashSet<Places>();
        }

        public int Id { get; set; }
        public int? StateId { get; set; }
        public string CityKey { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public virtual States State { get; set; }
        public virtual ICollection<Places> Places { get; set; }
    }
}
