﻿using System;
using System.Collections.Generic;

namespace LocalSearchEngineAPI.Models
{
    public partial class Users
    {
        public Users()
        {
            Subscriptions = new HashSet<Subscriptions>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Subscriptions> Subscriptions { get; set; }
    }
}
