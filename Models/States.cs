﻿using System;
using System.Collections.Generic;

namespace LocalSearchEngineAPI.Models
{
    public partial class States
    {
        public States()
        {
            Cities = new HashSet<Cities>();
        }

        public int Id { get; set; }
        public string StateKey { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Cities> Cities { get; set; }
    }
}
