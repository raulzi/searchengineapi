﻿using System;
using System.Collections.Generic;

namespace LocalSearchEngineAPI.Models
{
    public partial class Subscriptions
    {
        public int IdUser { get; set; }
        public int IdTopic { get; set; }

        public virtual Topics IdTopicNavigation { get; set; }
        public virtual Users IdUserNavigation { get; set; }
    }
}
