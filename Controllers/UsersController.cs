﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LocalSearchEngineAPI.Models;
using LocalSearchEngineAPI.DTOs;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using LocalSearchEngineAPI.Controllers.Request;

namespace LocalSearchEngineAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly SG_DBContext _context;
        private readonly IConfiguration _config;

        public UsersController(SG_DBContext context, IConfiguration Configuration)
        {
            _context = context;
            _config = Configuration;
        }

        // GET: api/Users
        /// <summary>
        /// Description: Get all users
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<UsersDTO> GetUsers()
        {
            return Mapper.Map<IEnumerable<UsersDTO>>(_context.Users.OrderByDescending(x => x.Id));
        }

        // GET: api/Users/User/5
        /// <summary>
        /// Description: Get User by id
        /// Developer: Raul Ziranda 
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("User/{userId}")]
        public IEnumerable<UsersDTO> GetUser_User([FromRoute] int id)
        {
            return Mapper.Map<IEnumerable<UsersDTO>>(_context.Users.Where(x => x.Id == id).OrderByDescending(x => x.Name));
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

            if (users == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<UsersDTO>(users));
        }

        // POST: api/Login
        /// <summary>
        /// Description: Login user
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest credentials)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.Users.SingleOrDefaultAsync(m => 
                m.Username == credentials.username 
                && m.Password == credentials.password);

            if (user == null)
            {
                return NotFound();
            }

            var token = GenerateJWT();

            return Ok(new { user = Mapper.Map<UsersDTO>(user), token = token });

        }

        // PUT: api/Users/5
        /// <summary>
        /// Description: Edit user by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsers([FromRoute] int id,[FromBody] UsersDTO users)
        {
            users.Subscriptions = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != users.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Users>(users)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        /// <summary>
        /// Description: Add new user
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostUsers([FromBody] UsersDTO users)
        {
            users.Subscriptions = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = Mapper.Map<Users>(users);

            _context.Users.Add(user);
            try
            {
                await _context.SaveChangesAsync();
                users.Id = user.Id;
            }
            catch (DbUpdateException)
            {
                if (UsersExists(users.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUsers", new { id = user.Id }, users);
        }

        // DELETE: api/Users/5
        /// <summary>
        /// Description: Delete user by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (users == null)
            {
                return NotFound();
            }

            _context.Users.Remove(users);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<UsersDTO>(users));
        }

        /// <summary>
        /// Description: Check if user exists
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool UsersExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        /// <summary>
        /// Description: Generate JWT 
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <returns></returns>
        private string GenerateJWT()
        {
            var issuer = _config["JWTManagement:Issuer"];
            var audience = _config["JWTManagement:Audience"];
            var expiry = DateTime.Now.AddMinutes(300);
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWTManagement:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(issuer: issuer,
                audience: audience,
                expires: DateTime.Now.AddMinutes(300),
                signingCredentials: credentials);

            var tokenHandler = new JwtSecurityTokenHandler();
            var stringToken = tokenHandler.WriteToken(token);

            return stringToken;
        }
    }
}
