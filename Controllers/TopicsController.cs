﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LocalSearchEngineAPI.Models;
using AutoMapper;
using LocalSearchEngineAPI.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace LocalSearchEngineAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TopicsController : ControllerBase
    {
        private readonly SG_DBContext _context;

        public TopicsController(SG_DBContext context)
        {
            _context = context;
        }

        // GET: api/Topics
        /// <summary>
        /// Description: Get all topics
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<TopicsDTO> GetTopics()
        {
            return Mapper.Map<IEnumerable<TopicsDTO>>(_context.Topics.OrderByDescending(x => x.Id));
        }

        // GET: api/Topics/Topic/5
        /// <summary>
        /// Description: Get topic by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpGet("Topic/{topicId}")]
        public IEnumerable<TopicsDTO> GetTopic_Topic([FromRoute] int topicId)
        {
            return Mapper.Map<IEnumerable<TopicsDTO>>(_context.Places.Where(x => x.Id == topicId).OrderByDescending(x => x.Id));
        }

        // GET: api/Topics/5
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTopics([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var topics = await _context.Topics.SingleOrDefaultAsync(m => m.Id == id);

            if (topics == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<TopicsDTO>(topics));
        }

        // PUT: api/Topics/5
        /// <summary>
        /// Description: Edit topic by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="topics"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTopics([FromRoute] int id,[FromBody] TopicsDTO topics)
        {
            topics.Places = null;
            topics.Subscriptions = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != topics.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Topics>(topics)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Topics
        /// <summary>
        /// Description: Add new topic
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="topics"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostTopics([FromBody] TopicsDTO topics)
        {
            topics.Places = null;;
            topics.Subscriptions = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var topic = Mapper.Map<Topics>(topics);

            _context.Topics.Add(topic);
            try
            {
                await _context.SaveChangesAsync();
                topics.Id = topic.Id;
            }
            catch (DbUpdateException)
            {
                if (TopicsExists(topics.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTopics", new { id = topic.Id }, topics);
        }

        // DELETE: api/Topics/5
        /// <summary>
        /// Description: Delete topic by id+
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTopics([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var topics = await _context.Topics.SingleOrDefaultAsync(m => m.Id == id);
            if (topics == null)
            {
                return NotFound();
            }

            _context.Topics.Remove(topics);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<TopicsDTO>(topics));
        }

        /// <summary>
        /// Description: Check if topic exists
        /// Developer: Raul Ziranda
        /// Date: 19/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool TopicsExists(int id)
        {
            return _context.Topics.Any(e => e.Id == id);
        }
    }
}
