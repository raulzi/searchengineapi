﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LocalSearchEngineAPI.Models;
using AutoMapper;
using LocalSearchEngineAPI.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace LocalSearchEngineAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SubscriptionsController : ControllerBase
    {
        private readonly SG_DBContext _context;

        public SubscriptionsController(SG_DBContext context)
        {
            _context = context;
        }

        // GET: api/Subscriptions
        /// <summary>
        /// Description: Get all subscriptions
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<SubscriptionsDTO> GetSubscriptions()
        {
            return Mapper.Map<IEnumerable<SubscriptionsDTO>>(_context.Subscriptions.OrderByDescending(x => x.IdUser));
        }

        // GET: api/Subscriptios/User/5
        /// <summary>
        /// Description: Get subscriptions by user id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        [HttpGet("User/{idUser}")]
        public IActionResult GetSubscriptionsByUser([FromRoute] int idUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscriptions =  Mapper.Map<IEnumerable<SubscriptionsDTO>>(_context.Subscriptions.Where(x => x.IdUser == idUser).OrderByDescending(x => x.IdTopic));

            if (subscriptions == null)
            {
                return NotFound();
            }

            return Ok(subscriptions);

        }

        // GET: api/Subscriptions/Topic/5
        /// <summary>
        /// Description: Get subscriptions by topic id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="idTopic"></param>
        /// <returns></returns>
        [HttpGet("Topic/{idTopic}")]
        public IActionResult GetSubscriptionsByTopic([FromRoute] int idTopic)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscriptions = Mapper.Map<IEnumerable<SubscriptionsDTO>>(_context.Subscriptions.Where(x => x.IdTopic == idTopic));

            if (subscriptions == null)
            {
                return NotFound();
            }

            return Ok(subscriptions);
        }

        // GET: api/Subscriptions/5/1
        /// <summary>
        /// Description: Get subscription by user id and topic id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="idTopic"></param>
        /// <returns></returns>
        [HttpGet("{idUser}/{idTopic}")]
        public async Task<IActionResult> GetSubscriptions([FromRoute] int idUser,[FromRoute] int idTopic)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscriptions = await _context.Subscriptions.SingleOrDefaultAsync(x => x.IdUser == idUser && x.IdTopic == idTopic);

            if (subscriptions == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<SubscriptionsDTO>(subscriptions));
        }

        // POST: api/Subscriptions
        /// <summary>
        /// Description: Add new subscription
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="subscriptions"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostSubscriptions([FromBody] SubscriptionsDTO subscriptions)
        {
            subscriptions.IdTopicNavigation = null;
            subscriptions.IdUserNavigation = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscription = Mapper.Map<Subscriptions>(subscriptions);

            _context.Subscriptions.Add(subscription);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubscriptionsExists(subscriptions.IdUser, subscriptions.IdTopic))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSubscriptions", new { idUser = subscriptions.IdUser, IdTopic = subscriptions.IdTopic }, subscriptions);
        }

        // DELETE: api/Subscriptions/5/1
        /// <summary>
        /// Description: Delete subscription by user id and topic id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="idTopic"></param>
        /// <returns></returns>
        [HttpDelete("{idUser}/{idTopic}")]
        public async Task<IActionResult> DeleteSubscriptions([FromRoute] int idUser,[FromRoute] int idTopic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscriptions = await _context.Subscriptions.SingleOrDefaultAsync(x => x.IdUser == idUser && x.IdTopic == idTopic);
            if (subscriptions == null)
            {
                return NotFound();
            }

            _context.Subscriptions.Remove(subscriptions);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<SubscriptionsDTO>(subscriptions));
        }

        /// <summary>
        /// Description: Check if subscription exists by user id and topic id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="idTopic"></param>
        /// <returns></returns>
        private bool SubscriptionsExists(int idUser, int idTopic)
        {
            return _context.Subscriptions.Any(e => e.IdUser == idUser && e.IdTopic == idTopic);
        }
    }
}
