﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LocalSearchEngineAPI.Models;
using AutoMapper;
using LocalSearchEngineAPI.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace LocalSearchEngineAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PlacesController : ControllerBase
    {
        private readonly SG_DBContext _context;

        public PlacesController(SG_DBContext context)
        {
            _context = context;
        }

        // GET: api/Places
        /// <summary>
        /// Description: Get all places
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<PlacesDTO> GetPlaces()
        {
            return Mapper.Map<IEnumerable<PlacesDTO>>(_context.Places.OrderByDescending(x => x.PlaceName));
        }

        // GET: api/Places/Filter
        /// <summary>
        /// Description: Get places by locality and topic
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="idLocality"></param>
        /// <param name="idTopic"></param>
        /// <returns></returns>
        [HttpGet("Filter/{idLocality}/{idTopic?}")]
        public IActionResult GetPlacesByFilter([FromRoute] int idLocality, [FromRoute] int idTopic = 0)
        {
            IEnumerable<PlacesDTO> places = null;

            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (idTopic == 0)
            {
                places = Mapper.Map<IEnumerable<PlacesDTO>>(_context.Places.Where(x => x.IdLocality == idLocality).OrderByDescending(x => x.IdTopic));
            }
            else
            {
                places = Mapper.Map<IEnumerable<PlacesDTO>>(_context.Places.Where(x => x.IdLocality == idLocality && x.IdTopic == idTopic));
            }

            if (places == null)
            {
                return NotFound();
            }

            return Ok(places);

        }

        // GET: api/Places/Place/5
        /// <summary>
        /// Description: Get place by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="placeId"></param>
        /// <returns></returns>
        [HttpGet("Place/{placeId}")]
        public IEnumerable<PlacesDTO> GetPlace_Place([FromRoute] int placeId)
        {
            return Mapper.Map<IEnumerable<PlacesDTO>>(_context.Places.Where(x => x.Id == placeId).OrderByDescending(x => x.Id));
        }

        // GET: api/Places/5
        /// <summary>
        /// Description: Get place by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlaces([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var places = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);

            if (places == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<PlacesDTO>(places));
        }

        // PUT: api/Places/5
        /// <summary>
        /// Description: Edit place by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="places"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlaces([FromRoute] int id,[FromBody] PlacesDTO places)
        {
            places.IdLocalityNavigation = null;
            places.IdTopicNavigation = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != places.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Places>(places)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlacesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Places
        /// <summary>
        /// Description: Add new place
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="places"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostPlaces([FromBody] PlacesDTO places)
        {
            places.IdLocalityNavigation = null;
            places.IdTopicNavigation = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var place = Mapper.Map<Places>(places);

            _context.Places.Add(place);

            try
            {
                await _context.SaveChangesAsync();
                places.Id = place.Id;
            }
            catch (DbUpdateException)
            {
                if (PlacesExists(place.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPlaces", new { id = place.Id }, places);
        }

        // DELETE: api/Places/5
        /// <summary>
        /// Description: Delete place by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlaces([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var places = await _context.Places.SingleOrDefaultAsync(m => m.Id == id);
            if (places == null)
            {
                return NotFound();
            }

            _context.Places.Remove(places);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<PlacesDTO>(places));
        }

        /// <summary>
        /// Description: Check if place exists by id
        /// Developer: Raul
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool PlacesExists(int id)
        {
            return _context.Places.Any(e => e.Id == id);
        }
    }
}
