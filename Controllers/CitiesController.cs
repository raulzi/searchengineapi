﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LocalSearchEngineAPI.Models;
using AutoMapper;
using LocalSearchEngineAPI.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace LocalSearchEngineAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CitiesController : ControllerBase
    {
        private readonly SG_DBContext _context;

        public CitiesController(SG_DBContext context)
        {
            _context = context;
        }

        // GET: api/Cities
        /// <summary>
        /// Description: Get all citites
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<CitiesDTO> GetCities()
        {
            return Mapper.Map<IEnumerable<CitiesDTO>>(_context.Cities.OrderByDescending(x => x.CityKey));
        }

        // GET: api/Cities/City/5
        [HttpGet("City/{cityId}")]
        public IEnumerable<CitiesDTO> GetCity_City([FromRoute] int cityId)
        {
            return Mapper.Map<IEnumerable<CitiesDTO>>(_context.Cities.Where(x => x.Id == cityId).OrderByDescending(x => x.CityKey));
        }


        // GET: api/Cities/5
        /// <summary>
        /// Description: Get city by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCities([FromRoute] int id)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cities = await _context.Cities.SingleOrDefaultAsync(m => m.Id == id);

            if (cities == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<CitiesDTO>(cities));
        }

        // PUT: api/Cities/5
        /// <summary>
        /// Description: Edit city by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cities"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCities([FromRoute] int id,[FromBody] CitiesDTO cities)
        {
            cities.State = null;
            cities.Places = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cities.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Cities>(cities)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CitiesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cities
        /// <summary>
        /// Description: Add new city
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="cities"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostCities([FromBody] CitiesDTO cities)
        {
            cities.State = null;
            cities.Places = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var city = Mapper.Map<Cities>(cities);

            _context.Cities.Add(city);

            try
            {
                await _context.SaveChangesAsync();
                cities.Id = city.Id;
            }
            catch (DbUpdateException)
            {
                if (CitiesExists(city.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCities", new { id = city.Id }, cities);
        }

        // DELETE: api/Cities/5
        /// <summary>
        /// Description: Delete city by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCities([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cities = await _context.Cities.SingleOrDefaultAsync(m => m.Id == id);
            if (cities == null)
            {
                return NotFound();
            }

            _context.Cities.Remove(cities);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<CitiesDTO>(cities));
        }

        /// <summary>
        /// Description: Check if city exits by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CitiesExists(int id)
        {
            return _context.Cities.Any(e => e.Id == id);
        }
    }
}
