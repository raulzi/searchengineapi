﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LocalSearchEngineAPI.Models;
using LocalSearchEngineAPI.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace LocalSearchEngineAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StatesController : ControllerBase
    {
        private readonly SG_DBContext _context;

        public StatesController(SG_DBContext context)
        {
            _context = context;
        }

        // GET: api/States
        /// <summary>
        /// Description: Get all states
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<StatesDTO> GetStates()
        {
            return Mapper.Map<IEnumerable<StatesDTO>>(_context.States.OrderByDescending(x => x.StateKey));
        }

        // GET: api/States/State/5
        [HttpGet("State/{stateId}")]
        public IEnumerable<StatesDTO> GetState_State([FromRoute] int stateId)
        {
            return Mapper.Map<IEnumerable<StatesDTO>>(_context.Places.Where(x => x.Id == stateId).OrderByDescending(x => x.Id));
        }

        // GET: api/States/5
        /// <summary>
        /// Description: Get state by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetStates([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var states = await _context.States.SingleOrDefaultAsync(m => m.Id == id);

            if (states == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<StatesDTO>(states));
        }

        // PUT: api/States/5
        /// <summary>
        /// Description: Edit state by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="states"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStates([FromRoute] int id,[FromBody] StatesDTO states)
        {
            states.Cities = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            if (id != states.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<States>(states)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StatesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/States
        /// <summary>
        /// Description: Add new state
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="states"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostStates([FromBody] StatesDTO states)
        {
            states.Cities = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var state = Mapper.Map<States>(states);

            _context.States.Add(state);
            try
            {
                await _context.SaveChangesAsync();
                states.Id = state.Id;
            }
            catch (DbUpdateException)
            {
                if (StatesExists(states.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetStates", new { id = state.Id }, states);
        }

        // DELETE: api/States/5
        /// <summary>
        /// Description: Delete state by id
        /// Developer: Raul Ziranda
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStates([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var states = await _context.States.SingleOrDefaultAsync(m => m.Id == id);
            if (states == null)
            {
                return NotFound();
            }

            _context.States.Remove(states);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<StatesDTO>(states));
        }

        /// <summary>
        /// Description: Check if place exists by id
        /// Developer: Raul
        /// Date: 16/08/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool StatesExists(int id)
        {
            return _context.States.Any(e => e.Id == id);
        }
    }
}
