﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocalSearchEngineAPI.DTOs
{
    public class TopicsDTO
    {
        public int Id { get; set; }
        public string TopicDescription { get; set; }
        public bool Active { get; set; }

        public List<PlacesDTO> Places { get; set; }
        public List<SubscriptionsDTO> Subscriptions { get; set; }
    }
}
