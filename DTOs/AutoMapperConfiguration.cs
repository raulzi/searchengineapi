﻿using AutoMapper;
using LocalSearchEngineAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocalSearchEngineAPI.DTOs
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Cities, CitiesDTO>()
                .ForMember(x => x.State, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<Places, PlacesDTO>()
                .ForMember(x => x.IdLocalityNavigation, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<States, StatesDTO>()
                .ReverseMap();

                cfg.CreateMap<Subscriptions, SubscriptionsDTO>()
                .ForMember(x => x.IdUserNavigation, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<Topics, TopicsDTO>()
                .ReverseMap();

                cfg.CreateMap<Users, UsersDTO>()
                .ForMember(x => x.Subscriptions, o => o.Ignore())
                .ReverseMap();

            });
        }
    }
}
