﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocalSearchEngineAPI.DTOs
{
    public class PlacesDTO
    {
        public int Id { get; set; }
        public string PlaceName { get; set; }
        public string PlaceDescription { get; set; }
        public string Address { get; set; }
        public string ImageB64 { get; set; }
        public int? IdTopic { get; set; }
        public int? IdLocality { get; set; }
        public bool Active { get; set; }

        public CitiesDTO IdLocalityNavigation { get; set; }
        public TopicsDTO IdTopicNavigation { get; set; }
    }
}
