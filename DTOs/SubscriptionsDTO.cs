﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocalSearchEngineAPI.DTOs
{
    public class SubscriptionsDTO
    {
        public int IdUser { get; set; }
        public int IdTopic { get; set; }

        public TopicsDTO IdTopicNavigation { get; set; }
        public UsersDTO IdUserNavigation { get; set; }
    }
}
