﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocalSearchEngineAPI.DTOs
{
    public class StatesDTO
    {
        public int Id { get; set; }
        public string StateKey { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public bool Active { get; set; }

        public List<CitiesDTO> Cities { get; set; }
    }
}
