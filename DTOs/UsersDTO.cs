﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocalSearchEngineAPI.DTOs
{
    public class UsersDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }

        public List<SubscriptionsDTO> Subscriptions { get; set; }
    }
}
