﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocalSearchEngineAPI.DTOs
{
    public class CitiesDTO
    {
        public int Id { get; set; }
        public int? StateId { get; set; }
        public string CityKey { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public StatesDTO State { get; set; }
        public List<PlacesDTO> Places { get; set; }


        
    }
}
